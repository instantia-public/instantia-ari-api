<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Throwable $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Throwable $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        $rendered = parent::render($request, $exception);

        $message = ($this->isJSON($exception->getMessage()))
            ? json_decode($exception->getMessage())
            : $exception->getMessage();

        // this will avoid having a nested repetitive "message" key in the response
        if(!empty($message->message)){
            $message = ["error" => $message->message];
        }

        return response()->json([
            'error' => [
                'code' => $rendered->getStatusCode(),
                'message' => $message,
            ]
        ], $rendered->getStatusCode());
    }

    /**
     * @param $string
     * @return bool
     */
    private function isJSON($string)
    {
        return is_string($string)
        && is_array(json_decode($string, true))
        && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }
}
