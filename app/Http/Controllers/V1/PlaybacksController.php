<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;

class PlaybacksController extends AbstractController
{

    /**
     * @OA\Get(
     *      path="/playbacks/{playbackId}",
     *      operationId="playbacksGet",
     *      tags={"Playbacks"},
     *      summary="Get a playback's details.",
     *      description="Get a playback's details.",
     *     @OA\Parameter(
     *         name="playbackId",
     *         in="path",
     *         description="Playback's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $playbackId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function get($playbackId)
    {
        return $this->getAsterisk('/playbacks/' . $playbackId);
    }

    /**
     * @OA\DELETE(
     *      path="/playbacks/{playbackId}",
     *      operationId="playbacksStop",
     *      tags={"Playbacks"},
     *      summary="Stop a playback.",
     *      description="Stop a playback.",
     *     @OA\Parameter(
     *         name="playbackId",
     *         in="path",
     *         description="Playback's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $playbackId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function stop($playbackId)
    {
        return $this->deleteAsterisk('/playbacks/' . $playbackId);
    }

    /**
     * @OA\POST(
     *      path="/playbacks/{playbackId}/control",
     *      operationId="playbacksControl",
     *      tags={"Playbacks"},
     *      summary="Control a playback.",
     *      description="Control a playback.",
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="operation",
     *                          description="(required) Operation to perform on the playback. Allowed values: restart, pause, unpause, reverse, forward.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $playbackId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function control(Request $request, $playbackId)
    {
        $this->validation($request, [
            'queryParams.operation' => 'required|string|in:restart,pause,unpause,reverse,forward'
        ]);

        return $this->postAsterisk('/playbacks/' . $playbackId . '/control', $request->all());
    }
}
