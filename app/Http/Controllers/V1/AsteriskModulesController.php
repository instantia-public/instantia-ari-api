<?php

namespace App\Http\Controllers\V1;

class AsteriskModulesController extends AbstractController
{

    /**
     * AsteriskModulesController constructor.
     */
    public function __construct()
    {
        $this->endpointPrefix = "/asterisk";
        parent::__construct();
    }


    /**
     * @OA\Get(
     *      path="/asterisk/modules",
     *      operationId="asteriskModules",
     *      tags={"Asterisk Modules"},
     *      summary="List Asterisk modules.",
     *      description="List Asterisk modules.",
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function index()
    {
        return $this->getAsterisk('/modules');
    }

    /**
     * @OA\Get(
     *      path="/asterisk/modules/{module}",
     *      operationId="asteriskShowModule",
     *      tags={"Asterisk Modules"},
     *      summary="Get Asterisk module information.",
     *      description="Get Asterisk module information.",
     *     @OA\Parameter(
     *         name="module",
     *         in="path",
     *         description="Module's name",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param $module
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show($module)
    {
        return $this->getAsterisk('/modules/' . $module);
    }

    /**
     * @OA\POST(
     *      path="/asterisk/modules/{module}",
     *      operationId="asteriskCreateModule",
     *      tags={"Asterisk Modules"},
     *      summary="Load an Asterisk module.",
     *      description="Load an Asterisk module.",
     *     @OA\Parameter(
     *         name="module",
     *         in="path",
     *         description="Module's name",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param $module
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function create($module)
    {
        return $this->postAsterisk('/modules/' . $module);
    }

    /**
     * @OA\PUT(
     *      path="/asterisk/modules/{module}",
     *      operationId="asteriskUpdateModule",
     *      tags={"Asterisk Modules"},
     *      summary="Reload an Asterisk module.",
     *      description="Reload an Asterisk module.",
     *     @OA\Parameter(
     *         name="module",
     *         in="path",
     *         description="Module's name",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param $module
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function putModule($module)
    {
        return $this->putAsterisk('/modules/' . $module);
    }

    /**
     * @OA\DELETE(
     *      path="/asterisk/modules/{module}",
     *      operationId="asteriskDeleteModule",
     *      tags={"Asterisk Modules"},
     *      summary="Unload an Asterisk module.",
     *      description="Unload an Asterisk module.",
     *     @OA\Parameter(
     *         name="module",
     *         in="path",
     *         description="Module's name",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=204, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param $module
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($module)
    {
        return $this->deleteAsterisk('/modules/' . $module);
    }

}
