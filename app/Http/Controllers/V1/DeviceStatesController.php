<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;

class DeviceStatesController extends AbstractController
{

    /**
     * @OA\Get(
     *      path="/deviceStates",
     *      operationId="deviceStatesList",
     *      tags={"Device States"},
     *      summary="List all ARI controlled device states.",
     *      description="List all ARI controlled device states.",
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function list()
    {
        return $this->getAsterisk('/deviceStates');
    }

    /**
     * @OA\Get(
     *      path="/deviceStates/{deviceName}",
     *      operationId="deviceStatesGet",
     *      tags={"Device States"},
     *      summary="Retrieve the current state of a device.",
     *      description="Retrieve the current state of a device.",
     *     @OA\Parameter(
     *         name="deviceName",
     *         in="path",
     *         description="Name of the device.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $deviceName
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function get($deviceName)
    {
        return $this->getAsterisk('/deviceStates/' . $deviceName);
    }

    /**
     * @OA\PUT(
     *      path="/deviceState/{deviceName}",
     *      operationId="deviceStatesUpdate",
     *      tags={"Device States"},
     *      summary="Filter application events types.",
     *      description="Change the state of a device controlled by ARI. (Note - implicitly creates the device state).",
     * @OA\Parameter(
     *         name="deviceName",
     *         in="path",
     *         description="Name of the device.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="deviceState",
     *                          description="Device state value. Allowed values (case sensitive): NOT_INUSE, INUSE, BUSY, INVALID, UNAVAILABLE, RINGING, RINGINUSE, ONHOLD",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     * @OA\Response(response=200, description="Successful operation"),
     * @OA\Response(response=500, description="Server Error"),
     * @OA\Response(response=422, description="Unprocessable Entity"),
     * @OA\Response(response=405, description="Method Not Allowed"),
     * @OA\Response(response=404, description="Bad request"),
     * @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param Request $request
     * @param $deviceName
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, $deviceName)
    {
        $this->validation($request, [
            'queryParams.deviceState' => 'required|string|in:NOT_INUSE,INUSE,BUSY,INVALID,UNAVAILABLE,RINGING,RINGINUSE,ONHOLD',
        ]);

        return $this->putAsterisk('/deviceStates/' . $deviceName, $request->all());
    }

    /**
     * @OA\DELETE(
     *      path="/deviceStates/{deviceName}",
     *      operationId="deviceStatesDelete",
     *      tags={"Device States"},
     *      summary="Destroy a device-state controlled by ARI.",
     *      description="Destroy a device-state controlled by ARI.",
     *     @OA\Parameter(
     *         name="deviceName",
     *         in="path",
     *         description="Name of the device.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=204, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $deviceName
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($deviceName)
    {
        return $this->deleteAsterisk('/deviceStates/' . $deviceName);
    }

}
