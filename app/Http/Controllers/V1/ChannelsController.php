<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;

class ChannelsController extends AbstractController
{

    protected $asteriskApiPrefix;

    /**
     * @OA\Get(
     *      path="/channels",
     *      operationId="channelsList",
     *      tags={"Channels"},
     *      summary="List all active channels in Asterisk.",
     *      description="List all active channels in Asterisk.",
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function list()
    {
        return $this->getAsterisk('/channels');
    }

    /**
     * @OA\Get(
     *      path="/channels/{channel}",
     *      operationId="showChannel",
     *      tags={"Channels"},
     *      summary="Channel details.",
     *      description="Channel details.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's Id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function get($channel)
    {
        return $this->getAsterisk('/channels/' . $channel);
    }

    /**
     * @OA\POST(
     *      path="/channels",
     *      operationId="channelsCreateOriginate",
     *      tags={"Channels"},
     *      summary="Create a new channel (originate).",
     *      description="Create a new channel (originate). The new channel is created immediately and a snapshot of it returned.
     *      If a Stasis application is provided it will be automatically subscribed to the originated channel for further events and updates.",
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="endpoint",
     *                          description="(required) Endpoint to call.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="extension",
     *                          description="The extension to dial after the endpoint answers. Mutually exclusive with 'app'.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="context",
     *                          description="The context to dial after the endpoint answers. If omitted, uses 'default'. Mutually exclusive with 'app'.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="priority",
     *                          description="The priority to dial after the endpoint answers. If omitted, uses 1. Mutually exclusive with 'app'.",
     *                          type="long",
     *                      ),
     *                      @OA\Property(
     *                          property="label",
     *                          description="The label to dial after the endpoint answers. Will supersede 'priority' if provided. Mutually exclusive with 'app'.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="app",
     *                          description="The application that is subscribed to the originated channel. When the channel is answered, it will be passed to this Stasis application. Mutually exclusive with 'context', 'extension', 'priority', and 'label'.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="appArgs",
     *                          description="The application arguments to pass to the Stasis application provided by 'app'. Mutually exclusive with 'context', 'extension', 'priority', and 'label'.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="callerId",
     *                          description="CallerID to use when dialing the endpoint or extension.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="timeout",
     *                          description="Timeout (in seconds) before giving up dialing, or -1 for no timeout. Default: 30",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="channelId",
     *                          description="The unique id to assign the channel on creation.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="otherChannelId",
     *                          description="The unique id to assign the second channel when using local channels.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="originator",
     *                          description="The unique id of the channel which is originating this one.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="formats",
     *                          description="The format name capability list to use if originator is not specified. Ex. 'ulaw,slin16'. Format names can be found with 'core show codecs'.",
     *                          type="string",
     *                      )
     *                  ),
     *                  @OA\Property(
     *                      property="bodyParams",
     *                      description="Body Parameters",
     *                      type="object",
     *                      @OA\Property(
     *                         property="variables",
     *                         description="containers - The 'variables' key holds variable key/value pairs to set on the channel on creation. Other keys in the body object are interpreted as query parameters. Ex. { 'endpoint': 'SIP/Alice', 'variables': { 'CALLERID(name)': 'Alice' } }",
     *                         type="object"
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function originate(Request $request)
    {
        $this->validation($request, [
            'queryParams.endpoint' => 'required|string', //@todo: according to Asterisk docs, this is the only required param (but, to be confirmed)
            'queryParams.extension' => 'required|string',
            'queryParams.context' => 'required|string',
            'queryParams.priority' => 'required|int',
            'queryParams.label' => 'required|string',
            'queryParams.app' => 'required|string',
            'queryParams.appArgs' => 'required|string',
            'queryParams.callerId' => 'required|string',
            'queryParams.timeout' => 'required|int',
            'queryParams.channelId' => 'required|string',
            'queryParams.otherChannelId' => 'required|string',
            'queryParams.originator' => 'required_without:queryParams.formats|string',
            'queryParams.formats' => 'required_without:queryParams.originator|string',
        ]);

        return $this->postAsterisk('/channels', $request->all());
    }

    /**
     * @OA\POST(
     *      path="/channels/create",
     *      operationId="channelsCreateChannel",
     *      tags={"Channels"},
     *      summary="Create channel.",
     *      description="Create channel.",
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="endpoint",
     *                          description="(required) Endpoint for channel communication",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="app",
     *                          description="(required) Stasis Application to place channel into.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="appArgs",
     *                          description="The application arguments to pass to the Stasis application provided by 'app'. Mutually exclusive with 'context', 'extension', 'priority', and 'label'.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="channelId",
     *                          description="The unique id to assign the channel on creation.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="otherChannelId",
     *                          description="The unique id to assign the second channel when using local channels.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="originator",
     *                          description="Unique ID of the calling channel.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="formats",
     *                          description="The format name capability list to use if originator is not specified. Ex. 'ulaw,slin16'. Format names can be found with 'core show codecs'.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function create(Request $request)
    {
        $this->validation($request, [
            'queryParams.endpoint' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.app' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.appArgs' => 'required|string',
            'queryParams.channelId' => 'required|string',
            'queryParams.otherChannelId' => 'required|string',
            'queryParams.originator' => 'required_without:queryParams.formats|string',
            'queryParams.formats' => 'required_without:queryParams.originator|string',
        ]);

        return $this->postAsterisk('/channels/create', $request->all());
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}",
     *      operationId="channelsCreateOriginateWithId",
     *      tags={"Channels"},
     *      summary="Create a new channel (originate with id).",
     *      description="Create a new channel (originate with id). The new channel is created immediately and a snapshot of it returned. If a Stasis application is provided it will be automatically subscribed to the originated channel for further events and updates.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="The unique id to assign the channel on creation.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="endpoint",
     *                          description="(required) Endpoint to call.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="extension",
     *                          description="The extension to dial after the endpoint answers. Mutually exclusive with 'app'.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="context",
     *                          description="The context to dial after the endpoint answers. If omitted, uses 'default'. Mutually exclusive with 'app'.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="priority",
     *                          description="The priority to dial after the endpoint answers. If omitted, uses 1. Mutually exclusive with 'app'.",
     *                          type="long",
     *                      ),
     *                      @OA\Property(
     *                          property="label",
     *                          description="The label to dial after the endpoint answers. Will supersede 'priority' if provided. Mutually exclusive with 'app'.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="app",
     *                          description="The application that is subscribed to the originated channel. When the channel is answered, it will be passed to this Stasis application. Mutually exclusive with 'context', 'extension', 'priority', and 'label'.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="appArgs",
     *                          description="The application arguments to pass to the Stasis application provided by 'app'. Mutually exclusive with 'context', 'extension', 'priority', and 'label'.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="callerId",
     *                          description="CallerID to use when dialing the endpoint or extension.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="timeout",
     *                          description="Timeout (in seconds) before giving up dialing, or -1 for no timeout. Default: 30",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="channelId",
     *                          description="The unique id to assign the channel on creation.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="otherChannelId",
     *                          description="The unique id to assign the second channel when using local channels.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="originator",
     *                          description="The unique id of the channel which is originating this one.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="formats",
     *                          description="The format name capability list to use if originator is not specified. Ex. 'ulaw,slin16'. Format names can be found with 'core show codecs'.",
     *                          type="string",
     *                      )
     *                  ),
     *                  @OA\Property(
     *                      property="bodyParams",
     *                      description="Body Parameters",
     *                      type="object",
     *                      @OA\Property(
     *                         property="variables",
     *                         description="containers - The 'variables' key holds variable key/value pairs to set on the channel on creation. Other keys in the body object are interpreted as query parameters. Ex. { 'endpoint': 'SIP/Alice', 'variables': { 'CALLERID(name)': 'Alice' } }",
     *                         type="object"
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function originateWithId(Request $request, $channel)
    {
        $this->validation($request, [
            'queryParams.endpoint' => 'required|string', //@todo: according to Asterisk docs, this is the only required param (but, to be confirmed)
            'queryParams.extension' => 'required|string',
            'queryParams.context' => 'required|string',
            'queryParams.priority' => 'required|int',
            'queryParams.label' => 'required|string',
            'queryParams.app' => 'required|string',
            'queryParams.appArgs' => 'required|string',
            'queryParams.callerId' => 'required|string',
            'queryParams.timeout' => 'required|int',
            'queryParams.channelId' => 'required|string',
            'queryParams.otherChannelId' => 'required|string',
            'queryParams.originator' => 'required_without:queryParams.formats|string',
            'queryParams.formats' => 'required_without:queryParams.originator|string',
        ]);

        return $this->postAsterisk('/channels/' . $channel, $request->all());
    }

    /**
     * @OA\DELETE(
     *      path="/channels/{channel}",
     *      operationId="channelDelete",
     *      tags={"Channels"},
     *      summary="Delete (i.e. hangup) a channel.",
     *      description="Delete (i.e. hangup) a channel.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=204, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function hangup($channel)
    {
        return $this->deleteAsterisk('/channels/' . $channel);
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/continue",
     *      operationId="channelsContinueWithId",
     *      tags={"Channels"},
     *      summary="Exit application; continue execution in the dialplan.",
     *      description="Exit application; continue execution in the dialplan.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="context",
     *                          description="The context to continue to.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="extension",
     *                          description="The extension to continue to.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="priority",
     *                          description="The priority to continue to.",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="label",
     *                          description="The label to continue to - will supersede 'priority' if both are provided.",
     *                          type="string",
     *                      ),
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function continueInDialplan(Request $request, $channel)
    {
        $this->validation($request, [
            'queryParams.context' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.extension' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.priority' => 'required|int',
            'queryParams.label' => 'required|string'
        ]);

        return $this->postAsterisk('/channels/' . $channel . '/continue', $request->all());
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/move",
     *      operationId="channelsMoveWithId",
     *      tags={"Channels"},
     *      summary="Move the channel from one Stasis application to another.",
     *      description="Move the channel from one Stasis application to another.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="app",
     *                          description=" (required) The channel will be passed to this Stasis application.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="appArgs",
     *                          description="The application arguments to pass to the Stasis application provided by 'app'.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function move(Request $request, $channel)
    {
        $this->validation($request, [
            'queryParams.app' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.appArgs' => 'required|string'
        ]);

        return $this->postAsterisk('/channels/' . $channel . '/move', $request->all());
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/redirect",
     *      operationId="channelsRedirectWithId",
     *      tags={"Channels"},
     *      summary="Redirect the channel to a different location.",
     *      description="Redirect the channel to a different location.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="endpoint",
     *                          description="(required) The endpoint to redirect the channel to.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function redirect(Request $request, $channel)
    {
        $this->validation($request, [
            'queryParams.endpoint' => 'required|string'
        ]);

        return $this->postAsterisk('/channels/' . $channel . '/redirect', $request->all());
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/answer",
     *      operationId="channelsAnswerWithId",
     *      tags={"Channels"},
     *      summary="Answer a channel.",
     *      description="Answer a channel.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function answer($channel)
    {
        return $this->postAsterisk('/channels/' . $channel . '/answer');
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/ring",
     *      operationId="channelsRingWithId",
     *      tags={"Channels"},
     *      summary="Indicate ringing to a channel.",
     *      description="Indicate ringing to a channel.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function ring($channel)
    {
        return $this->postAsterisk('/channels/' . $channel . '/ring');
    }

    /**
     * @OA\DELETE(
     *      path="/channels/{channel}/ring",
     *      operationId="channelsDeleteRingWithId",
     *      tags={"Channels"},
     *      summary="Stop ringing indication on a channel if locally generated.",
     *      description="Stop ringing indication on a channel if locally generated.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=204, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function ringStop($channel)
    {
        return $this->deleteAsterisk('/channels/' . $channel . '/ring');
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/dtmf",
     *      operationId="channelsDtmfWithId",
     *      tags={"Channels"},
     *      summary="Send provided DTMF to a given channel.",
     *      description="Send provided DTMF to a given channel.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="dtmf",
     *                          description="DTMF To send.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="before",
     *                          description="Amount of time to wait before DTMF digits (specified in milliseconds) start. Default 100.",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="duration",
     *                          description="Length of each DTMF digit (specified in milliseconds). Default 100.",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="after",
     *                          description="Amount of time to wait after DTMF digits (specified in milliseconds) end.",
     *                          type="int",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function sendDTMF(Request $request, $channel)
    {
        $this->validation($request, [
            'queryParams.dtmf' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.before' => 'required|int',
            'queryParams.between' => 'required|int',
            'queryParams.after' => 'required|int',
        ]);

        return $this->postAsterisk('/channels/' . $channel . '/dtmf', $request->all());
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/mute",
     *      operationId="channelsMuteWithId",
     *      tags={"Channels"},
     *      summary="Mute a channel.",
     *      description="Mute a channel.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="direction",
     *                          description="Direction in which to mute audio. Default: both. Allowed values: both, in, out.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function mute(Request $request, $channel)
    {
        $this->validation($request, [
            'queryParams.direction' => 'required|string|in:both,in,out'
        ]);

        return $this->postAsterisk('/channels/' . $channel . '/mute', $request->all());
    }

    /**
     * @OA\DELETE(
     *      path="/channels/{channel}/mute",
     *      operationId="channelsDeleteMuteWithId",
     *      tags={"Channels"},
     *      summary="Unmute a channel.",
     *      description="Unmute a channel.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=204, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function unmute($channel)
    {
        return $this->deleteAsterisk('/channels/' . $channel . '/mute');
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/hold",
     *      operationId="channelsHoldWithId",
     *      tags={"Channels"},
     *      summary="Hold a channel.",
     *      description="Hold a channel.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function hold($channel)
    {
        return $this->postAsterisk('/channels/' . $channel . '/hold');
    }

    /**
     * @OA\DELETE(
     *      path="/channels/{channel}/hold",
     *      operationId="channelsDeleteHoldWithId",
     *      tags={"Channels"},
     *      summary="Remove a channel from hold.",
     *      description="Remove a channel from hold.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=204, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function unhold($channel)
    {
        return $this->deleteAsterisk('/channels/' . $channel . '/hold');
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/moh",
     *      operationId="channelsMohWithId",
     *      tags={"Channels"},
     *      summary="Play music on hold to a channel.",
     *      description="Play music on hold to a channel. Using media operations such as /play on a channel playing MOH in this manner will suspend MOH without resuming automatically. If continuing music on hold is desired, the stasis application must reinitiate music on hold.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="mohClass",
     *                          description="Music on hold class to use.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function startMoh(Request $request, $channel)
    {
        $this->validation($request, [
            'queryParams.mohClass' => 'required|string'
        ]);

        return $this->postAsterisk('/channels/' . $channel . '/moh', $request->all());
    }

    /**
     * @OA\DELETE(
     *      path="/channels/{channel}/moh",
     *      operationId="channelsDeleteMohWithId",
     *      tags={"Channels"},
     *      summary="Stop playing music on hold to a channel.",
     *      description="Stop playing music on hold to a channel.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=204, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function stopMoh($channel)
    {
        return $this->deleteAsterisk('/channels/' . $channel . '/moh');
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/silence",
     *      operationId="channelsSilenceWithId",
     *      tags={"Channels"},
     *      summary="Play silence to a channel.",
     *      description="Play silence to a channel. Using media operations such as /play on a channel playing silence in this manner will suspend silence without resuming automatically.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function startSilence($channel)
    {
        return $this->postAsterisk('/channels/' . $channel . '/silence');
    }

    /**
     * @OA\DELETE(
     *      path="/channels/{channel}/silence",
     *      operationId="channelsDeleteSilenceWithId",
     *      tags={"Channels"},
     *      summary="Stop playing silence to a channel.",
     *      description="Stop playing silence to a channel.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=204, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function stopSilence($channel)
    {
        return $this->deleteAsterisk('/channels/' . $channel . '/silence');
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/play",
     *      operationId="channelsPlayWithId",
     *      tags={"Channels"},
     *      summary="Start playback of media.",
     *      description="Start playback of media. The media URI may be any of a number of URI's. Currently sound:, recording:, number:, digits:, characters:, and tone: URI's are supported. This operation creates a playback resource that can be used to control the playback of media (pause, rewind, fast forward, etc.).",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="media",
     *                          description="(required) Media URIs to play. Allows comma separated values.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="lang",
     *                          description="For sounds, selects language for sound.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="offsetms",
     *                          description="Number of milliseconds to skip before playing. Only applies to the first URI if multiple media URIs are specified.",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="skipms",
     *                          description="Number of milliseconds to skip for forward/reverse operations. Default: 3000.",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="playbackId",
     *                          description="Playback ID.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function play(Request $request, $channel)
    {
        $this->validation($request, [
            'queryParams.media' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.lang' => 'required|string',
            'queryParams.offsetms' => 'required|int',
            'queryParams.skipms' => 'required|int',
            'queryParams.playbackId' => 'required|string',
        ]);

        return $this->postAsterisk('/channels/' . $channel . '/play', $request->all());
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/play/{playbackId}",
     *      operationId="channelsPlayWithIdAndPlaybackId",
     *      tags={"Channels"},
     *      summary="Start playback of media and specify the playbackId.",
     *      description="Start playback of media and specify the playbackId. The media URI may be any of a number of URI's. Currently sound:, recording:, number:, digits:, characters:, and tone: URI's are supported. This operation creates a playback resource that can be used to control the playback of media (pause, rewind, fast forward, etc.).",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="playbackId",
     *         in="path",
     *         description="Playback id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="media",
     *                          description="(required) Media URIs to play. Allows comma separated values.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="lang",
     *                          description="For sounds, selects language for sound.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="offsetms",
     *                          description="Number of milliseconds to skip before playing. Only applies to the first URI if multiple media URIs are specified.",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="skipms",
     *                          description="Number of milliseconds to skip for forward/reverse operations. Default: 3000.",
     *                          type="int",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $channel
     * @param $playbackId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function playWithId(Request $request, $channel, $playbackId)
    {
        $this->validation($request, [
            'queryParams.media' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.lang' => 'required|string',
            'queryParams.offsetms' => 'required|int',
            'queryParams.skipms' => 'required|int',
        ]);

        return $this->postAsterisk('/channels/' . $channel . '/play/' . $playbackId, $request->all());
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/record",
     *      operationId="channelsPlayWithId",
     *      tags={"Channels"},
     *      summary="Start a recording.",
     *      description="Start a recording. Record audio from a channel. Note that this will not capture audio sent to the channel. The bridge itself has a record feature if that's what you want.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="name",
     *                          description="(required) Recording's filename.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="format",
     *                          description="(required) Format to encode audio in.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="maxDurationSeconds",
     *                          description=" Maximum duration of the recording, in seconds. 0 for no limit. Allowed range: Min: 0; Max: None.",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="skipms",
     *                          description="Maximum duration of silence, in seconds. 0 for no limit. Allowed range: Min: 0; Max: None.",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="ifExists",
     *                          description="Action to take if a recording with the same name already exists. Default: fail. Allowed values: fail, overwrite, append.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="beep",
     *                          description="Play beep when recording begins.",
     *                          type="boolean",
     *                      ),
     *                      @OA\Property(
     *                          property="terminateOn",
     *                          description="DTMF input to terminate recording. Default: none. Allowed values: none, any, *, #.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function record(Request $request, $channel)
    {
        $this->validation($request, [
            'queryParams.name' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.format' => 'required|string',
            'queryParams.maxDurationSeconds' => 'required|int|min:0',
            'queryParams.maxSilenceSeconds' => 'required|int|min:0',
            'queryParams.ifExists' => 'required|string|in:fail,overwrite,append',
            'queryParams.beep' => 'required|boolean',
            'queryParams.terminateOn' => 'required|string|in:none,any,*,#',
        ]);

        return $this->postAsterisk('/channels/' . $channel . '/record', $request->all());
    }

    /**
     * @OA\Get(
     *      path="/channels/{channel}/variable/{variable}",
     *      operationId="showChannel",
     *      tags={"Channels"},
     *      summary="Channel details.",
     *      description="Channel details.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's Id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="variable",
     *         in="path",
     *         description="The channel variable or function to get.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $channel
     * @param $variable
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getChannelVar($channel, $variable)
    {
        return $this->getAsterisk('/channels/' . $channel . '/variable?variable=    ' . $variable);
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/variable",
     *      operationId="channelsWithIdSetChannelVar",
     *      tags={"Channels"},
     *      summary="Set the value of a channel variable or function.",
     *      description="Set the value of a channel variable or function.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="variable",
     *                          description="(required) The channel variable or function to set.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="value",
     *                          description="The value to set the variable to.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function setChannelVar(Request $request, $channel)
    {
        $this->validation($request, [
            'queryParams.variable' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.value' => 'required|string',
        ]);

        return $this->postAsterisk('/channels/' . $channel . '/variable', $request->all());
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/snoop",
     *      operationId="channelsWithIdSnoop",
     *      tags={"Channels"},
     *      summary="Start snooping.",
     *      description="Start snooping. Snoop (spy/whisper) on a specific channel.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="spy",
     *                          description="Direction of audio to spy on. Default: none. Allowed values: none, both, out, in.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="whisper",
     *                          description="Direction of audio to whisper into. Default: none. Allowed values: none, both, out, in.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="app",
     *                          description="(required) Application the snooping channel is placed into.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="appArgs",
     *                          description="The application arguments to pass to the Stasis application.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="snoopId",
     *                          description="Unique ID to assign to snooping channel.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function snoopChannel(Request $request, $channel)
    {
        $this->validation($request, [
            'queryParams.spy' => 'required|string|in:none,both,out,in',
            'queryParams.whisper' => 'required|string|in:none,both,out,in',
            'queryParams.app' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.appArgs' => 'required|string',
            'queryParams.snoopId' => 'required|string',
        ]);

        return $this->postAsterisk('/channels/' . $channel . '/snoop', $request->all());
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/snoop/{snoopId}",
     *      operationId="channelsWithIdSnoopWithId",
     *      tags={"Channels"},
     *      summary="Start snooping.",
     *      description="Start snooping. Snoop (spy/whisper) on a specific channel.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="snoopId",
     *         in="path",
     *         description="Unique ID to assign to snooping channel",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="spy",
     *                          description="Direction of audio to spy on. Default: none. Allowed values: none, both, out, in.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="whisper",
     *                          description="Direction of audio to whisper into. Default: none. Allowed values: none, both, out, in.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="app",
     *                          description="(required) Application the snooping channel is placed into.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="appArgs",
     *                          description="The application arguments to pass to the Stasis application.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $channel
     * @param $snoopId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function snoopChannelWithId(Request $request, $channel, $snoopId)
    {
        $this->validation($request, [
            'queryParams.spy' => 'required|string|in:none,both,out,in',
            'queryParams.whisper' => 'required|string|in:none,both,out,in',
            'queryParams.app' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.appArgs' => 'required|string'
        ]);

        return $this->postAsterisk('/channels/' . $channel . '/snoop/' . $snoopId, $request->all());
    }

    /**
     * @OA\POST(
     *      path="/channels/{channel}/dial",
     *      operationId="channelsWithIdDial",
     *      tags={"Channels"},
     *      summary="Dial a created channel.",
     *      description="Dial a created channel.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="caller",
     *                          description="Channel ID of caller.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="timeout",
     *                          description="Dial timeout. Allowed range: Min: 0; Max: None.",
     *                          type="int",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function dial(Request $request, $channel)
    {
        $this->validation($request, [
            'queryParams.caller' => 'required|string',
            'queryParams.timeout' => 'required|int|min:0',
        ]);

        return $this->postAsterisk('/channels/' . $channel . '/dial', $request->all());
    }

    /**
     * @OA\Get(
     *      path="/channels/{channel}/rtpstatistics",
     *      operationId="showChannelWithIdRtpStatistics",
     *      tags={"Channels"},
     *      summary="RTP stats on a channel.",
     *      description="RTP stats on a channel.",
     *     @OA\Parameter(
     *         name="channel",
     *         in="path",
     *         description="Channel's Id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $channel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function rtpstatistics($channel)
    {
        return $this->getAsterisk('/channels/' . $channel . '/rtp_statistics');
    }

    /**
     * @OA\POST(
     *      path="/channels/externalMedia",
     *      operationId="channelsExternalMedia",
     *      tags={"Channels"},
     *      summary="Start an External Media session.",
     *      description="Start an External Media session. Create a channel to an External Media source/sink.",
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="channelId",
     *                          description="The unique id to assign the channel on creation.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="app",
     *                          description="(required) Stasis Application to place channel into.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="external_host",
     *                          description="(required) Hostname/ip:port of external host.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="encapsulation",
     *                          description="Payload encapsulation protocol. Default: rtp. Allowed values: rtp.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="transport",
     *                          description="ransport protoco. Default: udp. Allowed values: udp.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="connection_type",
     *                          description="Connection type (client/server). Default: client. Allowed values: client.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="format",
     *                          description="(required) Format to encode audio in.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="direction",
     *                          description="External media direction. Default: both. Allowed values: both.",
     *                          type="string",
     *                      )
     *                  ),
     *                  @OA\Property(
     *                      property="bodyParams",
     *                      description="Body Parameters",
     *                      type="object",
     *                      @OA\Property(
     *                         property="variables",
     *                         description="containers - The 'variables' key holds variable key/value pairs to set on the channel on creation. Other keys in the body object are interpreted as query parameters. Ex. { 'endpoint': 'SIP/Alice', 'variables': { 'CALLERID(name)': 'Alice' } }",
     *                         type="object"
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function externalMedia(Request $request)
    {
        $this->validation($request, [
            'queryParams.channelId' => 'required|string',
            'queryParams.app' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.external_host' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.encapsulation' => 'required|string|in:rtp',
            'queryParams.transport' => 'required|string|in:udp',
            'queryParams.connection_type' => 'required|string|in:client',
            'queryParams.format' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.direction' => 'required|string|in:both',
        ]);

        return $this->postAsterisk('/channels/externalMedia', $request->all());
    }

}
