<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;

class ApplicationsController extends AbstractController
{

    /**
     * @OA\Get(
     *      path="/applications",
     *      operationId="applicationsList",
     *      tags={"Applications"},
     *      summary="List all applications.",
     *      description="List all applications.",
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function list()
    {
        return $this->getAsterisk('/applications');
    }

    /**
     * @OA\Get(
     *      path="/applications/{applicationName}",
     *      operationId="showChannel",
     *      tags={"Applications"},
     *      summary="Get details of an application.",
     *      description="Get details of an application.",
     *     @OA\Parameter(
     *         name="applicationName",
     *         in="path",
     *         description="Application's name.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $applicationName
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function get($applicationName)
    {
        return $this->getAsterisk('/applications/' . $applicationName);
    }

    /**
     * @OA\POST(
     *      path="/applications/{applicationName}/subscription",
     *      operationId="applicationsSubscription",
     *      tags={"Applications"},
     *      summary="Subscribe an application to a event source. Returns the state of the application after the subscriptions have changed",
     *      description="Subscribe an application to a event source. Returns the state of the application after the subscriptions have changed",
     *     @OA\Parameter(
     *         name="applicationName",
     *         in="path",
     *         description="Application's name.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="eventSource",
     *                          description="(required) URI for event source (channel:{channelId}, bridge:{bridgeId}, endpoint:{tech}[/{resource}], deviceState:{deviceName}. Allows comma separated values.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $applicationName
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function subscribe(Request $request, $applicationName)
    {
        $this->validation($request, [
            'queryParams.eventSource' => 'required|string',
        ]);

        return $this->postAsterisk('/applications/' . $applicationName . '/subscription', $request->all());
    }

    /**
     * @OA\DELETE(
     *      path="/applications/{applicationName}/unsubscribe",
     *      operationId="applicationsUnsubscribe",
     *      tags={"Applications"},
     *      summary="Unsubscribe an application from an event source. Returns the state of the application after the subscriptions have changed.",
     *      description="Unsubscribe an application from an event source. Returns the state of the application after the subscriptions have changed.",
     *     @OA\Parameter(
     *         name="applicationName",
     *         in="path",
     *         description="Application's name.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=204, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $applicationName
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function unsubscribe($applicationName)
    {
        return $this->deleteAsterisk('/applications/' . $applicationName . '/unsubscribe');
    }

    /**
     * @OA\PUT(
     *      path="/applications/{applicationName}/eventFilter",
     *      operationId="applicationsEventFilter",
     *      tags={"Applications"},
     *      summary="Filter application events types.",
     *      description="
    Filter application events types. Allowed and/or disallowed event type filtering can be done. The body (parameter) should specify a JSON key/value object that describes the type of event filtering needed. One, or both of the following keys can be designated:

    'allowed' - Specifies an allowed list of event types
    'disallowed' - Specifies a disallowed list of event types

    Further, each of those key's value should be a JSON array that holds zero, or more JSON key/value objects. Each of these objects must contain the following key with an associated value:

    'type' - The type name of the event to filter

    The value must be the string name (case sensitive) of the event type that needs filtering. For example:

    { 'allowed': [ { 'type': 'StasisStart' }, { 'type': 'StasisEnd' } ] }

    As this specifies only an allowed list, then only those two event type messages are sent to the application. No other event messages are sent.

    The following rules apply:

    If the body is empty, both the allowed and disallowed filters are set empty.
    If both list types are given then both are set to their respective values (note, specifying an empty array for a given type sets that type to empty).
    If only one list type is given then only that type is set. The other type is not updated.
    An empty 'allowed' list means all events are allowed.
    An empty 'disallowed' list means no events are disallowed.
    Disallowed events take precedence over allowed events if the event type is specified in both lists.",
     * @OA\Parameter(
     *         name="applicationName",
     *         in="path",
     *         description="Application's name.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="filter",
     *                          description="Specify which event types to allow/disallow.",
     *                          type="object",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     * @OA\Response(response=200, description="Successful operation"),
     * @OA\Response(response=500, description="Server Error"),
     * @OA\Response(response=422, description="Unprocessable Entity"),
     * @OA\Response(response=405, description="Method Not Allowed"),
     * @OA\Response(response=404, description="Bad request"),
     * @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param Request $request
     * @param $applicationName
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function filter(Request $request, $applicationName)
    {
        $this->validation($request, [
            'queryParams.filter' => 'required|array',
        ]);

        return $this->putAsterisk('/applications/' . $applicationName . '/eventFilter', $request->all());
    }


}
