<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;

class MailboxesController extends AbstractController
{

    /**
     * @OA\Get(
     *      path="/mailboxes",
     *      operationId="mailboxesList",
     *      tags={"Mailboxes"},
     *      summary="List all mailboxes.",
     *      description="List all mailboxes.",
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function list()
    {
        return $this->getAsterisk('/mailboxes');
    }

    /**
     * @OA\Get(
     *      path="/mailboxes/{mailboxName}",
     *      operationId="mailboxesGet",
     *      tags={"Mailboxes"},
     *      summary="Retrieve the current state of a mailbox.",
     *      description="Retrieve the current state of a mailbox.",
     *     @OA\Parameter(
     *         name="mailboxName",
     *         in="path",
     *         description="Name of the mailbox",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $mailboxName
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function get($mailboxName)
    {
        return $this->getAsterisk('/mailboxes/' . $mailboxName);
    }

    /**
     * @OA\PUT(
     *      path="/mailboxes/{mailboxName}",
     *      operationId="mailboxesUpdate",
     *      tags={"Mailboxes"},
     *      summary="Change the state of a mailbox. (Note - implicitly creates the mailbox).",
     *      description="Change the state of a mailbox. (Note - implicitly creates the mailbox).",
     * @OA\Parameter(
     *         name="mailboxName",
     *         in="path",
     *         description="Name of the mailbox.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="oldMessages",
     *                          description=" (required) Count of old messages in the mailbox.",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="newMessages",
     *                          description=" (required) Count of new messages in the mailbox.",
     *                          type="int",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     * @OA\Response(response=200, description="Successful operation"),
     * @OA\Response(response=500, description="Server Error"),
     * @OA\Response(response=422, description="Unprocessable Entity"),
     * @OA\Response(response=405, description="Method Not Allowed"),
     * @OA\Response(response=404, description="Bad request"),
     * @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param Request $request
     * @param $mailboxName
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, $mailboxName)
    {
        $this->validation($request, [
            'queryParams.oldMessages' => 'required|int',
            'queryParams.newMessages' => 'required|int',
        ]);

        return $this->putAsterisk('/mailboxes/' . $mailboxName, $request->all());
    }

    /**
     * @OA\DELETE(
     *      path="/mailboxes/{mailboxName}",
     *      operationId="mailboxesDelete",
     *      tags={"Mailboxes"},
     *      summary="Destroy a mailbox.",
     *      description="Destroy a mailbox.",
     *     @OA\Parameter(
     *         name="mailboxName",
     *         in="path",
     *         description="Name of the mailbox.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=204, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $mailboxName
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($mailboxName)
    {
        return $this->deleteAsterisk('/mailboxes/' . $mailboxName);
    }

}
