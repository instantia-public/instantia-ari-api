<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;

class BridgesController extends AbstractController
{

    /**
     * @OA\Get(
     *      path="/bridges",
     *      operationId="brigesList",
     *      tags={"Bridges"},
     *      summary="List all active bridges in Asterisk.",
     *      description="List all active bridges in Asterisk.",
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function list()
    {
        return $this->getAsterisk('/bridges');
    }

    /**
     * @OA\POST(
     *      path="/bridges",
     *      operationId="bridgesCreateBridge",
     *      tags={"Bridges"},
     *      summary="Create a new bridge.",
     *      description="Create a new bridge.",
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="type",
     *                          description="Comma separated list of bridge type attributes (mixing, holding, dtmf_events, proxy_media, video_sfu).",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="bridgeId",
     *                          description="Unique ID to give to the bridge being created.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="name",
     *                          description="Name to give to the bridge being created.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function create(Request $request)
    {
        $this->validation($request, [
            'queryParams.type' => 'required|string' . $this->validateBridgeTypes($request->input('queryParams.type')), //@todo: according to Asterisk docs, this is a required param
            'queryParams.bridgeId' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.name' => 'required|string'
        ]);

        return $this->postAsterisk('/bridges', $request->all());
    }

    /**
     * @param string $bridgeType
     * @return string
     */
    private function validateBridgeTypes($bridgeType = '')
    {
        $results = '';

        $validTypes = [
            'mixing', 'holding', 'dtmf_events', 'proxy_media', 'video_sfu'
        ];

        $types = explode(',', $bridgeType);
        foreach ($types as $type) {
            if (!in_array(trim($type), $validTypes)) {
                $results = '|in:' . implode(',', $validTypes);
                continue;
            }
        }
        return $results;
    }

    /**
     * @OA\POST(
     *      path="/bridges/{bridgeId}",
     *      operationId="bridgesCreateBridgeWithId",
     *      tags={"Bridges"},
     *      summary="Create a new bridge or updates an existing one. This bridge persists until it has been shut down, or Asterisk has been shut down.",
     *      description="Create a new bridge or updates an existing one. This bridge persists until it has been shut down, or Asterisk has been shut down.",
     *     @OA\Parameter(
     *         name="bridgeId",
     *         in="path",
     *         description="Unique ID to give to the bridge being created.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="type",
     *                          description="Comma separated list of bridge type attributes (mixing, holding, dtmf_events, proxy_media, video_sfu) to set.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="name",
     *                          description="Set the name of the bridge.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $bridgeId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function createWithId(Request $request, $bridgeId)
    {

        $this->validation($request, [
            'queryParams.type' => 'required|string' . $this->validateBridgeTypes($request->input('queryParams.type')), //@todo: according to Asterisk docs, this is a required param
            'queryParams.name' => 'required|string'
        ]);

        return $this->postAsterisk('/bridges/' . $bridgeId, $request->all());
    }

    /**
     * @OA\Get(
     *      path="/bridges/{bridgeId}",
     *      operationId="brigesGet",
     *      tags={"Bridges"},
     *      summary="Get bridge details.",
     *      description="Get bridge details.",
     *     @OA\Parameter(
     *         name="bridgeId",
     *         in="path",
     *         description="Bridge's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $bridgeId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function get($bridgeId)
    {
        return $this->getAsterisk('/bridges/' . $bridgeId);
    }

    /**
     * @OA\DELETE(
     *      path="/bridges/{bridgeId}",
     *      operationId="brigesDestroy",
     *      tags={"Bridges"},
     *      summary="Shut down a bridge.",
     *      description="Shut down a bridge. If any channels are in this bridge, they will be removed and resume whatever they were doing beforehand.",
     *     @OA\Parameter(
     *         name="bridgeId",
     *         in="path",
     *         description="Bridge's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $bridgeId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($bridgeId)
    {
        return $this->deleteAsterisk('/bridges/' . $bridgeId);
    }

    /**
     * @OA\POST(
     *      path="/bridges/{bridgeId}/addChannel",
     *      operationId="bridgesAddChannel",
     *      tags={"Bridges"},
     *      summary="Add a channel to a bridge.",
     *      description="Add a channel to a bridge.",
     *     @OA\Parameter(
     *         name="bridgeId",
     *         in="path",
     *         description="Bridge's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="channel",
     *                          description="(required) Ids of channels to add to bridge. Allows comma separated values.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="role",
     *                          description="Channel's role in the bridge.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="absorbDTMF",
     *                          description="Absorb DTMF coming from this channel, preventing it to pass through to the bridge.",
     *                          type="boolean",
     *                      ),
     *                      @OA\Property(
     *                          property="mute",
     *                          description="Mute audio from this channel, preventing it to pass through to the bridge.",
     *                          type="boolean",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $bridgeId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addChannel(Request $request, $bridgeId)
    {
        $this->validation($request, [
            'queryParams.channel' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.role' => 'required|string', //@todo: according to Asterisk docs, this is a required param
            'queryParams.absorbDTMF' => 'required|boolean',
            'queryParams.mute' => 'required|boolean'
        ]);

        return $this->postAsterisk('/bridges/' . $bridgeId . '/addChannel', $request->all());
    }

    /**
     * @OA\POST(
     *      path="/bridges/{bridgeId}/removeChannel",
     *      operationId="brigesRemoveChannel",
     *      tags={"Bridges"},
     *      summary="Remove a channel from a bridge.",
     *      description="Remove a channel from a bridge.",
     *     @OA\Parameter(
     *         name="bridgeId",
     *         in="path",
     *         description="Bridge's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="channel",
     *                          description="(required) Ids of channels to remove from bridge. Allows comma separated values.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     * @OA\Response(response=200, description="Successful operation"),
     * @OA\Response(response=500, description="Server Error"),
     * @OA\Response(response=422, description="Unprocessable Entity"),
     * @OA\Response(response=405, description="Method Not Allowed"),
     * @OA\Response(response=404, description="Bad request"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $bridgeId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function removeChannel(Request $request, $bridgeId)
    {
        $this->validation($request, [
            'queryParams.channel' => 'required|string'
        ]);

        return $this->postAsterisk('/bridges/' . $bridgeId . '/removeChannel');
    }

    /**
     * @OA\POST(
     *      path="/bridges/{bridgeId}/videoSource/{channelId}",
     *      operationId="brigesSetVideoSource",
     *      tags={"Bridges"},
     *      summary="Set a channel as the video source in a multi-party mixing bridge.",
     *      description="Set a channel as the video source in a multi-party mixing bridge. This operation has no effect on bridges with two or fewer participants.",
     *     @OA\Parameter(
     *         name="bridgeId",
     *         in="path",
     *         description="Bridge's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="channelId",
     *         in="path",
     *         description="Channel's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\Response(response=200, description="Successful operation"),
     * @OA\Response(response=500, description="Server Error"),
     * @OA\Response(response=422, description="Unprocessable Entity"),
     * @OA\Response(response=405, description="Method Not Allowed"),
     * @OA\Response(response=404, description="Bad request"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $bridgeId
     * @param $channelId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function setVideoSource($bridgeId, $channelId)
    {
        return $this->postAsterisk('/bridges/' . $bridgeId . '/videoSource/' . $channelId);
    }

    /**
     * @OA\DELETE(
     *      path="/bridges/{bridgeId}/videoSource",
     *      operationId="brigesClearVideoSource",
     *      tags={"Bridges"},
     *      summary="Removes any explicit video source in a multi-party mixing bridge.",
     *      description="Removes any explicit video source in a multi-party mixing bridge. This operation has no effect on bridges with two or fewer participants. When no explicit video source is set, talk detection will be used to determine the active video stream.",
     *     @OA\Parameter(
     *         name="bridgeId",
     *         in="path",
     *         description="Bridge's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $bridgeId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function clearVideoSource($bridgeId)
    {
        return $this->deleteAsterisk('/bridges/' . $bridgeId . '/videoSource');
    }

    /**
     * @OA\POST(
     *      path="/bridges/{bridgeId}/moh",
     *      operationId="brigesStartMoh",
     *      tags={"Bridges"},
     *      summary="Play music on hold to a bridge or change the MOH class that is playing.",
     *      description="Play music on hold to a bridge or change the MOH class that is playing.",
     *     @OA\Parameter(
     *         name="bridgeId",
     *         in="path",
     *         description="Bridge's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="mohClass",
     *                          description="Channel's id",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     * @OA\Response(response=200, description="Successful operation"),
     * @OA\Response(response=500, description="Server Error"),
     * @OA\Response(response=422, description="Unprocessable Entity"),
     * @OA\Response(response=405, description="Method Not Allowed"),
     * @OA\Response(response=404, description="Bad request"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $bridgeId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function startMoh(Request $request, $bridgeId)
    {
        $this->validation($request, [
            'queryParams.mohClass' => 'required|string'
        ]);

        return $this->postAsterisk('/bridges/' . $bridgeId . '/moh');
    }

    /**
     * @OA\DELETE(
     *      path="/bridges/{bridgeId}/moh",
     *      operationId="bridgesStopMoh",
     *      tags={"Bridges"},
     *      summary="Stop playing music on hold to a bridge.",
     *      description="Stop playing music on hold to a bridge. This will only stop music on hold being played via POST bridges/{bridgeId}/moh.",
     *     @OA\Parameter(
     *         name="bridgeId",
     *         in="path",
     *         description="Bridge's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $bridgeId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function stopMoh($bridgeId)
    {
        return $this->deleteAsterisk('/bridges/' . $bridgeId . '/moh');
    }

    /**
     * @OA\POST(
     *      path="/bridges/{bridgeId}/play",
     *      operationId="bridgesPlay",
     *      tags={"Bridges"},
     *      summary="Start playback of media on a bridge.",
     *      description="Start playback of media on a bridge. The media URI may be any of a number of URI's. Currently sound:, recording:, number:, digits:, characters:, and tone: URI's are supported. This operation creates a playback resource that can be used to control the playback of media (pause, rewind, fast forward, etc.).",
     *     @OA\Parameter(
     *         name="bridgeId",
     *         in="path",
     *         description="Bridge's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="media",
     *                          description="(required) Media URIs to play. Allows comma separated values.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="lang",
     *                          description="For sounds, selects language for sound.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="offsetms",
     *                          description="Number of milliseconds to skip before playing. Only applies to the first URI if multiple media URIs are specified. Allowed range: Min: 0; Max: None",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="skipms",
     *                          description="Number of milliseconds to skip for forward/reverse operations. Default: 3000. Allowed range: Min: 0; Max: None",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="playbackId",
     *                          description="Playback Id",
     *                          type="string",
     *                      ),
     *                  )
     *              )
     *          )
     *      ),
     * @OA\Response(response=200, description="Successful operation"),
     * @OA\Response(response=500, description="Server Error"),
     * @OA\Response(response=422, description="Unprocessable Entity"),
     * @OA\Response(response=405, description="Method Not Allowed"),
     * @OA\Response(response=404, description="Bad request"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $bridgeId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function play(Request $request, $bridgeId)
    {
        $this->validation($request, [
            'queryParams.media' => 'required|string',
            'queryParams.lang' => 'required|string',
            'queryParams.offsetms' => 'required|int:min:0',
            'queryParams.skipms' => 'required|int|min:0',
            'queryParams.playbackId' => 'required|string',
        ]);

        return $this->postAsterisk('/bridges/' . $bridgeId . '/play');
    }

    /**
     * @OA\POST(
     *      path="/bridges/{bridgeId}/play/{playbackId}",
     *      operationId="bridgesPlayWithId",
     *      tags={"Bridges"},
     *      summary="Start playback of media on a bridge.",
     *      description="Start playback of media on a bridge. The media URI may be any of a number of URI's. Currently sound:, recording:, number:, digits:, characters:, and tone: URI's are supported. This operation creates a playback resource that can be used to control the playback of media (pause, rewind, fast forward, etc.).",
     *     @OA\Parameter(
     *         name="bridgeId",
     *         in="path",
     *         description="Bridge's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="playbackId",
     *         in="path",
     *         description="Playback ID",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="media",
     *                          description="(required) Media URIs to play. Allows comma separated values.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="lang",
     *                          description="For sounds, selects language for sound.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="offsetms",
     *                          description="Number of milliseconds to skip before playing. Only applies to the first URI if multiple media URIs are specified. Allowed range: Min: 0; Max: None",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="skipms",
     *                          description="Number of milliseconds to skip for forward/reverse operations. Default: 3000. Allowed range: Min: 0; Max: None",
     *                          type="int",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     * @OA\Response(response=200, description="Successful operation"),
     * @OA\Response(response=500, description="Server Error"),
     * @OA\Response(response=422, description="Unprocessable Entity"),
     * @OA\Response(response=405, description="Method Not Allowed"),
     * @OA\Response(response=404, description="Bad request"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $bridgeId
     * @param $playbackId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function playWithId(Request $request, $bridgeId, $playbackId)
    {
        $this->validation($request, [
            'queryParams.media' => 'required|string',
            'queryParams.lang' => 'required|string',
            'queryParams.offsetms' => 'required|int:min:0',
            'queryParams.skipms' => 'required|int|min:0'
        ]);

        return $this->postAsterisk('/bridges/' . $bridgeId . '/play/' . $playbackId);
    }

    /**
     * @OA\POST(
     *      path="/bridges/{bridgeId}/record",
     *      operationId="bridgesRecord",
     *      tags={"Bridges"},
     *      summary="Start a recording.",
     *      description="Start a recording. This records the mixed audio from all channels participating in this bridge.",
     *     @OA\Parameter(
     *         name="bridgeId",
     *         in="path",
     *         description="Bridge's id",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="name",
     *                          description="(required) Recording's filename.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="format",
     *                          description="(required) Format to encode audio in.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="maxDurationSeconds",
     *                          description="Maximum duration of the recording, in seconds. 0 for no limit. Allowed range: Min: 0; Max: None.",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="maxSilenceSeconds",
     *                          description="Maximum duration of silence, in seconds. 0 for no limit. Allowed range: Min: 0; Max: None.",
     *                          type="int",
     *                      ),
     *                      @OA\Property(
     *                          property="ifExists",
     *                          description="Action to take if a recording with the same name already exists. Default: fail. Allowed values: fail, overwrite, append.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="beep",
     *                          description="Play beep when recording begins.",
     *                          type="boolean",
     *                      ),
     *                      @OA\Property(
     *                          property="terminateOn",
     *                          description="DTMF input to terminate recording. Default: none. Allowed values: none, any, *, #",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     * @OA\Response(response=200, description="Successful operation"),
     * @OA\Response(response=500, description="Server Error"),
     * @OA\Response(response=422, description="Unprocessable Entity"),
     * @OA\Response(response=405, description="Method Not Allowed"),
     * @OA\Response(response=404, description="Bad request"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @param $bridgeId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function record(Request $request, $bridgeId)
    {
        $this->validation($request, [
            'queryParams.name' => 'required|string',
            'queryParams.format' => 'required|string',
            'queryParams.maxDurationSeconds' => 'required|int:min:0',
            'queryParams.maxSilenceSeconds' => 'required|int|min:0',
            'queryParams.ifExists' => 'required|string|in:fail,overwrite,append',
            'queryParams.beep' => 'required|boolean',
            'queryParams.terminateOn' => 'required|string|in:none,any,*,#'
        ]);

        return $this->postAsterisk('/bridges/' . $bridgeId . '/record');
    }
}
