<?php

namespace App\Http\Controllers\V1;

class AsteriskLoggingController extends AbstractController
{

    /**
     * AsteriskLoggingController constructor.
     */
    public function __construct()
    {
        $this->endpointPrefix = "/asterisk";
        parent::__construct();
    }


    /**
     * @OA\Get(
     *      path="/asterisk/logging",
     *      operationId="asteriskLogging",
     *      tags={"Asterisk Logging"},
     *      summary="Gets Asterisk log channel information.",
     *      description="Gets Asterisk log channel information.",
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function listLogChannels()
    {
        return $this->getAsterisk('/logging');
    }

    /**
     * @OA\POST(
     *      path="/asterisk/logging/{logChannelName}",
     *      operationId="asteriskLoggingAddLog",
     *      tags={"Asterisk Logging"},
     *      summary="Adds a log channel.",
     *      description="Adds a log channel.",
     *     @OA\Parameter(
     *         name="logChannelName",
     *         in="path",
     *         description="The log channel to add.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param $logChannelName
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addLog($logChannelName)
    {
        return $this->postAsterisk('/logging/' . $logChannelName);
    }

    /**
     * @OA\DELETE(
     *      path="/asterisk/logging/{logChannelName}",
     *      operationId="asteriskDeleteLog",
     *      tags={"Asterisk Logging"},
     *      summary="Deletes a log channel.",
     *      description="Deletes a log channel.",
     *     @OA\Parameter(
     *         name="logChannelName",
     *         in="path",
     *         description="Log channels name.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=204, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param $logChannelName
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteLog($logChannelName)
    {
        return $this->deleteAsterisk('/logging/' . $logChannelName);
    }

    /**
     * @OA\PUT(
     *      path="/asterisk/logging/{logChannelName}/rotate",
     *      operationId="asteriskRotateLog",
     *      tags={"Asterisk Logging"},
     *      summary="Rotates a log channel.",
     *      description="Rotates a log channel.",
     *     @OA\Parameter(
     *         name="logChannelName",
     *         in="path",
     *         description="Log channels name.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param $logChannelName
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function rotateLog($logChannelName)
    {
        return $this->putAsterisk('/logging/' . $logChannelName . '/rotate');
    }

}
