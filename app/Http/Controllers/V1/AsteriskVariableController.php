<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;

class AsteriskVariableController extends AbstractController
{

    /**
     * AsteriskVariableController constructor.
     */
    public function __construct()
    {
        $this->endpointPrefix = "/asterisk";
        parent::__construct();
    }

    /**
     * @OA\Get(
     *      path="/asterisk/variable/{variableName}",
     *      operationId="asteriskGetGlobalVar",
     *      tags={"Asterisk Variables"},
     *      summary="Get the value of a global variable.",
     *      description="Get the value of a global variable.",
     *     @OA\Parameter(
     *         name="variableName",
     *         in="path",
     *         description="The variable to get.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $variableName
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getGlobalVar($variableName)
    {
        return $this->getAsterisk('/variable?variable=' . $variableName);
    }

    /**
     * @OA\POST(
     *      path="/asterisk/variable",
     *      operationId="asteriskSetGlobalVar",
     *      tags={"Asterisk Variables"},
     *      summary="Set the value of a global variable.",
     *      description="Set the value of a global variable.",
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                      @OA\Property(
     *                          property="variable",
     *                          description="(required) The variable to set.",
     *                          type="string",
     *                      ),
     *                      @OA\Property(
     *                          property="value",
     *                          description="The value to set the variable to.",
     *                          type="string",
     *                      )
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function setGlobalVar(Request $request)
    {
        $this->validation($request, [
            'queryParams.variable' => 'required|string',
            'queryParams.value' => 'required|string'
        ]);

        return $this->postAsterisk('/variable', $request->all());
    }
}
