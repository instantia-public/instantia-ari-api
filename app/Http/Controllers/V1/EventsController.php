<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;

class EventsController extends AbstractController
{

    /**
     * @OA\Get(
     *      path="/events",
     *      operationId="eventsEventWebSocket",
     *      tags={"Events"},
     *      summary="WebSocket connection for events.",
     *      description="WebSocket connection for events.",
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function eventWebsocket()
    {
        return $this->getAsterisk('/events');
    }

    /**
     * @OA\POST(
     *      path="/userEvent/user/{eventName}",
     *      operationId="eventsUserEvent",
     *      tags={"Events"},
     *      summary="Generate a user event.",
     *      description="Generate a user event.",
     *     @OA\Parameter(
     *         name="eventName",
     *         in="path",
     *         description="Event name.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                  @OA\Property(
     *                      property="application",
     *                      description="(required) The name of the application that will receive this event.",
     *                      type="string",
     *                  ),
     *                  @OA\Property(
     *                      property="source",
     *                      description="URI for event source (channel:{channelId}, bridge:{bridgeId}, endpoint:{tech}/{resource}, deviceState:{deviceName}. Allows comma separated values.",
     *                      type="string",
     *                  ),
     *              ),
     *              @OA\Property(
     *                  property="bodyParams",
     *                  description="Body Parameters",
     *                  type="object",
     *                  @OA\Property(
     *                      property="variables",
     *                      description="containers - The 'variables' key in the body object holds custom key/value pairs to add to the user event. Ex. { 'variables': { 'key': 'value' } }",
     *                      type="object"
     *                  )
     *                )
     *             )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param Request $request
     * @param $eventName
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function userEvent(Request $request,$eventName)
    {

        $this->validation($request, [
            'queryParams.application' => 'required|string',
            'queryParams.source' => 'required|string',
            'bodyParams.variables' => 'required|array'
        ]);
        return $this->postAsterisk('/events/user/' . $eventName, $request->all());
    }

}
