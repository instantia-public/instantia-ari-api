<?php


namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;

class EndpointsController extends AbstractController
{
    /**
     * @OA\Get(
     *      path="/endpoints",
     *      operationId="endpointsList",
     *      tags={"Endpoints"},
     *      summary="List all endpoints.",
     *      description="List all endpoints.",
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function list()
    {
        return $this->getAsterisk('/endpoints');
    }

    /**
     * @OA\Get(
     *      path="/endpoints/{tech}",
     *      operationId="endpointsShowTech",
     *      tags={"Endpoints"},
     *      summary="List available endpoints for a given endpoint technology.",
     *      description="List available endpoints for a given endpoint technology.",
     *     @OA\Parameter(
     *         name="tech",
     *         in="path",
     *         description="Technology of the endpoints (sip,iax2,...)",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param $tech
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function listByTech($tech)
    {
        return $this->getAsterisk('/endpoints/' . $tech);
    }

    /**
     * @OA\Get(
     *      path="/endpoints/{tech}/{resource}",
     *      operationId="endpointsShowTechResource",
     *      tags={"Endpoints"},
     *      summary="Details for an endpoint.",
     *      description="Details for an endpoint.",
     *     @OA\Parameter(
     *         name="tech",
     *         in="path",
     *         description="Technology of the endpoints (sip,iax2,...)",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="resource",
     *         in="path",
     *         description="ID of the endpoint",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param $tech
     * @param $resource
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function get($tech, $resource)
    {
        //@todo: need to add validation here!
        return $this->getAsterisk('/endpoints/' . $tech . '/' . $resource);
    }

    /**
     * @OA\PUT(
     *      path="/endpoints/sendMessage",
     *      operationId="endpointPutSendMessageToTechOrEndpoint",
     *      tags={"Endpoints"},
     *      summary="Send a message to some technology URI or endpoint.",
     *      description="Send a message to some technology URI or endpoint.",
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                    @OA\Property(
     *                        property="to",
     *                        description="(required) The endpoint resource or technology specific URI to send the message to. Valid resources are sip, pjsip, and xmpp.",
     *                        type="string",
     *                    ),
     *                    @OA\Property(
     *                        property="from",
     *                        description="(required) The endpoint resource or technology specific identity to send this message from. Valid resources are sip, pjsip, and xmpp.",
     *                        type="string",
     *                    ),
     *                    @OA\Property(
     *                        property="body",
     *                        description="The body of the message",
     *                        type="string",
     *                    )
     *                 )
     *             )
     *         )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function sendMessage(Request $request)
    {
        $this->validation($request, [
            'queryParams.to' => 'required|string',
            'queryParams.from' => 'required|string',
            'queryParams.body' => 'required|string' //@todo: according to Asterisk docs, this is not required (to be confirmed)
        ]);

        return $this->putAsterisk('/endpoints/sendMessage', $request->all());
    }

    /**
     * @OA\PUT(
     *      path="/endpoints/{tech}/{resource}/sendMessage",
     *      operationId="endpointPutSendMessageToEndpointInTech",
     *      tags={"Endpoints"},
     *      summary="Send a message to some endpoint in a technology.",
     *      description="Send a message to some endpoint in a technology.",
     *     @OA\Parameter(
     *         name="tech",
     *         in="path",
     *         description="Technology of the endpoints (sip,iax2,...)",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="resource",
     *         in="path",
     *         description="ID of the endpoint",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     * @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *        @OA\Property(
     *         property="queryParams",
     *         description="Query Parameters",
     *         type="object",
     *                    @OA\Property(
     *                        property="from",
     *                        description="(required) The endpoint resource or technology specific identity to send this message from. Valid resources are sip, pjsip, and xmpp.",
     *                        type="string",
     *                    ),
     *                    @OA\Property(
     *                        property="body",
     *                        description="The body of the message",
     *                        type="string",
     *                    )
     *                 ),
     *                  @OA\Property(
     *                      property="bodyParams",
     *                      description="Body Parameters",
     *                      type="object",
     *                      @OA\Property(
     *                         property="variables",
     *                         description="containers",
     *                         type="object"
     *                      )
     *                  )
     *             )
     *         )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param Request $request
     * @param $tech
     * @param $resource
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function sendMessageToEndpoint(Request $request, $tech, $resource)
    {
        $this->validation($request, [
            'queryParams.from' => 'required|string',
            'queryParams.body' => 'required|string' //@todo: according to Asterisk docs, this is not required (to be confirmed)
        ]);

        return $this->putAsterisk('/endpoints/' . $tech . '/' . $resource . '/sendMessage', $request->all());
    }

}
