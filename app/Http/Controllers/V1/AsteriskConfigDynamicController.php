<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;

class AsteriskConfigDynamicController extends AbstractController
{

    /**
     * AsteriskConfigDynamicController constructor.
     */
    public function __construct()
    {
        $this->endpointPrefix = "/asterisk/config";
        parent::__construct();
    }

    /**
     * @OA\Get(
     *      path="/asterisk/config/dynamic/{configClass}/{objectType}/{id}",
     *      operationId="asteriskGetConfigDynamic",
     *      tags={"Asterisk Dynamic Configuration"},
     *      summary="Retrieve a dynamic configuration object.",
     *      description="Retrieve a dynamic configuration object.",
     *     @OA\Parameter(
     *         name="configClass",
     *         in="path",
     *         description="The configuration class containing dynamic configuration objects.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="objectType",
     *         in="path",
     *         description="The type of configuration object to retrieve.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The unique identifier of the object to retrieve.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $configClass
     * @param $objectType
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getObject($configClass, $objectType, $id)
    {
        return $this->getAsterisk('/dynamic/' . $configClass . '/' . $objectType . '/' . $id);
    }

    /**
     * @OA\PUT(
     *      path="/asterisk/config/dynamic/{configClass}/{bojectType}/{id}",
     *      operationId="asteriskPutConfigDynamic",
     *      tags={"Asterisk Dynamic Configuration"},
     *      summary="Create or update a dynamic configuration object.",
     *      description="Create or update a dynamic configuration object.",
     *     @OA\Parameter(
     *         name="configClass",
     *         in="path",
     *         description="The configuration class containing dynamic configuration objects.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="objectType",
     *         in="path",
     *         description="The type of configuration object to create or update.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The unique identifier of the object to create or update.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 type="object",
     *              @OA\Property(
     *                  property="bodyParams",
     *                  description="Body Parameters",
     *                  type="object",
     *                  @OA\Property(
     *                      property="fields",
     *                      description="containers - The body object should have a value that is a list of ConfigTuples, which provide the fields to update. Ex. [ { 'attribute': 'directmedia', 'value': 'false' } ]",
     *                      type="object"
     *                  )
     *                )
     *             )
     *          )
     *      ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     * @param Request $request
     * @param $configClass
     * @param $objectType
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function putModule(Request $request, $configClass, $objectType, $id)
    {

        $this->validation($request, [
            'bodyParams.fields' => 'required|array' //@todo: according to Asterisk docs, this is not required (to be confirmed)
        ]);
        return $this->putAsterisk('/dynamic/' . $configClass . '/' . $objectType . '/' . $id, $request->all());
    }

    /**
     * @OA\Delete(
     *      path="/asterisk/config/dynamic/{configClass}/{objectType}/{id}",
     *      operationId="asteriskDeleteConfigDynamic",
     *      tags={"Asterisk Dynamic Configuration"},
     *      summary="Delete a dynamic configuration object.",
     *      description="Delete a dynamic configuration object.",
     *     @OA\Parameter(
     *         name="configClass",
     *         in="path",
     *         description="The configuration class containing dynamic configuration objects.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="objectType",
     *         in="path",
     *         description="The type of configuration object to delete.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The unique identifier of the object to delete.",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @param $configClass
     * @param $objectType
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteObject($configClass, $objectType, $id)
    {
        return $this->deleteAsterisk('/dynamic/' . $configClass . '/' . $objectType . '/' . $id);
    }

}
