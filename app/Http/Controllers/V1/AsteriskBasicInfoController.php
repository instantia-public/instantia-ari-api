<?php

namespace App\Http\Controllers\V1;

class AsteriskBasicInfoController extends AbstractController
{

    /**
     * AsteriskBasicInfoController constructor.
     */
    public function __construct()
    {
        $this->endpointPrefix = "/asterisk";
        parent::__construct();
    }

    /**
     * @OA\Get(
     *      path="/asterisk/ping",
     *      operationId="asteriskPing",
     *      tags={"Asterisk Basic Information"},
     *      summary="Response pong message.",
     *      description="Response pong message.",
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function ping()
    {
        return $this->getAsterisk('/ping');
    }

    /**
     * @OA\Get(
     *      path="/asterisk/info",
     *      operationId="asteriskInfo",
     *      tags={"Asterisk Basic Information"},
     *      summary="Gets Asterisk system information.",
     *      description="Gets Asterisk system information.",
     *      @OA\Response(response=200, description="Successful operation"),
     *      @OA\Response(response=500, description="Server Error"),
     *      @OA\Response(response=422, description="Unprocessable Entity"),
     *      @OA\Response(response=405, description="Method Not Allowed"),
     *      @OA\Response(response=404, description="Bad request"),
     *      @OA\Response(response=401, description="Unauthorized"),
     *       security={
     *           {"Bearer Authorization": {}}
     *       }
     *     )
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function info()
    {
        return $this->getAsterisk('/info');
    }

}
