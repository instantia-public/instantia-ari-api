<?php

namespace App\Http\Controllers\V1;


use App\Http\Controllers\Controller;
use App\Services\ApiClientRequestService;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AbstractController extends Controller
{

    protected $asteriskServer;
    protected $asteriskApiPrefix;
    protected $ariApiPrefix = "/ari";
    protected $endpointPrefix = "";
    protected $asteriskApiHeaders;
    protected $asteriskApiOptions;

    /**
     * AbstractController constructor.
     */
    public function __construct()
    {
        $this->asteriskServer = env('ASTERISK_SERVER', 'http://localhost') . $this->ariApiPrefix . $this->endpointPrefix;
        $this->asteriskApiHeaders = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Basic ' . base64_encode(
                    env('ASTERISK_USERNAME', 'user') . ":" . env('ASTERISK_PASSWORD', 'password')
                )
        ];
        $this->asteriskApiOptions = [
            'verify' => false,
            'timeout' => 30
        ];
    }

    /**
     * @param Request $request
     * @param $rules
     */
    public function validation(Request $request, $rules)
    {
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            throw new HttpException(
                422, $validator->errors()
            );
        }
    }

    /**
     * @param $url
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function getAsterisk($url)
    {
        $client = new ApiClientRequestService;
        $response = $client->send(
            __METHOD__,
            $this->asteriskServer . $url,
            [],
            $this->asteriskApiHeaders,
            $this->asteriskApiOptions,
            'GET'
        );

        return response()->json($response);
    }

    /**
     * @param $url
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function postAsterisk($url, $data = [])
    {
        /**
         * Asterisk uses query parameters as well as body parameters, so here we prepare that data
         */
        if (!empty($data['queryParams'])) {
            $url = $url . "?" . http_build_query($data['queryParams']);
        }
        $bodyParams = (!empty($data['bodyParams'])) ? $data['bodyParams'] : [];

        $client = new ApiClientRequestService;
        $response = $client->send(
            __METHOD__,
            $this->asteriskServer . $url,
            $bodyParams,
            $this->asteriskApiHeaders,
            $this->asteriskApiOptions,
            'POST'
        );

        return response()->json($response, 201);
    }

    /**
     * @param $url
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function putAsterisk($url, $data = [])
    {
        /**
         * Asterisk uses query parameters as well as body parameters, so here we prepare that data
         */
        if (!empty($data['queryParams'])) {
            $url = $url . "?" . http_build_query($data['queryParams']);
        }
        $bodyParams = (!empty($data['bodyParams'])) ? $data['bodyParams'] : [];

        $client = new ApiClientRequestService;
        $response = $client->send(
            __METHOD__,
            $this->asteriskServer . $url,
            $bodyParams,
            $this->asteriskApiHeaders,
            $this->asteriskApiOptions,
            'PUT'
        );

        return response('', 200);
    }

    /**
     * @param $url
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteAsterisk($url)
    {
        $client = new ApiClientRequestService;
        $response = $client->send(
            __METHOD__,
            $this->asteriskServer . $url,
            [],
            $this->asteriskApiHeaders,
            $this->asteriskApiOptions,
            'DELETE'
        );

        return response('', 204);
    }

}
