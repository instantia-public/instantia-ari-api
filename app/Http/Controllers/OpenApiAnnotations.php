<?php

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Instantia ARI API - Asterisks API Gateway",
 *      description="PHP Library for Connecting with Asterisk Server via ARI API",
 *      @OA\Contact(
 *          name="Instantia LLC",
 *          url="http://instantia.io",
 *          email="luis.murcia@instantia.io"
 *      ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     ),
 *      termsOfService="http://api.instantia.io/#"
 * )
 */

/**
 * @OA\Server(
 *      url=L5_SWAGGER_CONST_HOST,
 *      description="API Auth Server"
 *  )
 *
 */

/**
 * @OA\SecurityScheme(
 *      securityScheme="Bearer Authorization",
 *      description="Provide the token obtained from calling the /oauth/token endpoint.",
 *      in="header",
 *      name="bearerAuth",
 *      type="http",
 *      scheme="bearer",
 *      bearerFormat="JWT",
 * ),
 */

/**
 * @OA\Tag(
 *     name="Instantia LLC",
 *     description="Some description about Instantia LLC",
 *     @OA\ExternalDocumentation(
 *         description="Find out more",
 *         url="http://api.instantia.io"
 *     )
 * )
 *
 */

/**
 * @OA\POST(
 *      path="/oauth/token",
 *      operationId="oauthToken",
 *      tags={"Authentication"},
 *      summary="Provides a Bearer Token to Authorize API calls.",
 *      description="Provides a Bearer Token to Authorize API calls.",
 * @OA\RequestBody(
 *         description="Input data format",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                      @OA\Property(
 *                          property="grant_type",
 *                          description="Oauth2 Grant Type",
 *                          type="string",
 *                      ),
 *                      @OA\Property(
 *                          property="client_id",
 *                          description="Client ID .",
 *                          type="string",
 *                      ),
 *                      @OA\Property(
 *                          property="client_secret",
 *                          description="Client Secret.",
 *                          type="string",
 *                      ),
 *                      @OA\Property(
 *                          property="scope",
 *                          description="Scope",
 *                          type="string",
 *                      )
 *                  )
 *              )
 *      ),
 *      @OA\Response(response=200, description="Successful operation"),
 *      @OA\Response(response=500, description="Server Error"),
 *      @OA\Response(response=400, description="Bad request")
 * )
 */
