![Instantia](resources/img/instantia-logo.png "Instantia")
# Instantia ARI API - Asterisk API Gateway

TODO: work in progress...

### What is Instantia ARI API?
Instantia ARI API is a Gateway Framework for creating ARI (Asterisk REST interface) Applications.

### What is Asterisk?
Asterisk is an open source framework for building communications applications. Asterisk turns an ordinary computer into a communications server. Asterisk powers IP PBX systems, VoIP gateways, conference servers and other custom solutions. It is used by small businesses, large businesses, call centers, carriers and government agencies, worldwide. Asterisk is free and open source.

### What can you do with Asterisk?
Asterisk is a framework for building multi-protocol, real-time communications applications and solutions. Asterisk is to realtime voice and video applications as Apache is to web applications: the underlying platform. Asterisk abstracts the complexities of communications protocols and technologies, allowing you to concentrate on creating innovative products and solutions.

You can use Asterisk to build communications applications, things like business phone systems (also known as IP PBXs), call distributors, VoIP gateways and conference bridges. Asterisk includes both low and high-level components that significantly simplify the process of building these complex applications. 

### What is ARI?
ARI is an asynchronous API that allows developers to build communications applications by exposing the raw primitive objects in Asterisk - channels, bridges, endpoints, media, etc. - through an intuitive REST interface. The state of the objects being controlled by the user are conveyed via JSON events over a WebSocket.

### ARI Fundamentals
ARI consists of three different pieces that are - for all intents and purposes - interrelated and used together. They are:

- A RESTful interface that a client uses to control resources in Asterisk.
- A WebSocket that conveys events in JSON about the resources in Asterisk to the client.
- The Stasis dialplan application that hands over control of a channel from Asterisk to the client.

All three pieces work together, allowing a developer to manipulate and control the fundamental resources in Asterisk and build their own communications application.

## Installation

Configure the `.env` file accordingly (rename the provided `.env.example`):

Install the dependency libraries via composer:
```bash
composer install
```

Generate the database tables:
```bash
php artisan migrate
```

Run the application
```bash
php -S localhost:8000 -t public
```

## Authentication
Authenticate using OAuth2 - Client Credentials grant type.

Generate encryption keys by running the following command:
```bash
php artisan passport:keys
```

Generate new client credentials by running the following command:
```bash
php artisan passport:client --password
```

## API Documentation
Find the complete API documentation here:

[Instantia API Swagger Docs](http://api.instantia.io/api/documentation)

### How to Authenticate in Swagger

In order to authenticate, you must first obtain a Access Token by calling the /oauth/token endpoint, that token will be automatically injected as a Header parameter in all the endpoint calls (as a Bearer Token).

 - Click on the "Try it Out" button:

![Authentication Try-Out](resources/img/authentication-tryout.png)

 - Provide the corresponding input data as follows (Note that client_id & client_secret will be provisioned by server administrator, or you can generate your own if working in other environment):

```json
{
  "grant_type": "client_credentials",
  "client_id": "client_id",
  "client_secret": "client_secret",
  "scope": "*"
}
```

![Authentication Try-Out](resources/img/authentication-execute.png)

 - Copy the access_token value from the response:

![Authentication Try-Out](resources/img/authentication-token.png)
 
 
 - Click on the Authorize button:
 
![Authentication Try-Out](resources/img/authentication-authorize.png) 
 
 - Paste the access_token value (obtained on previous step) into the "value" field, then press the "Authorize" button:

![Authentication Try-Out](resources/img/authentication-access-token-value.png) 

 - Finally, click on the "Close" button (this will make all lock icons in every endpoint turn active, indicating Authorization Header was properly setup):

![Authentication Try-Out](resources/img/authentication-access-token-close.png)

To generate an updated version of the Swagger documentation, rename and configure the `/config/swagger-lume.php.example` file, and then run the following command (DO NOT RUN THIS IN PROD):
```bash
php artisan swagger-lume:generate
``` 

## References
- [Asterisk v17 ARI Wiki](https://wiki.asterisk.org/wiki/display/AST/Asterisk+17+ARI)
- [Lumen](https://lumen.laravel.com/)
