<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//$router->post('register','UsersController@register');
$router->get('/', function ()  {
    return view('welcome');
});

$router->group([
    'namespace' => 'V1',
    'prefix' => 'api/v1/',
    'middleware' => 'client_credentials'
], function ($router) {
    $router->group([
        'prefix' => 'asterisk/'
    ], function($router) {
        $router->get('ping', 'AsteriskBasicInfoController@ping');
        $router->get('info', 'AsteriskBasicInfoController@info');

        //Modules:
        $router->get('modules', 'AsteriskModulesController@index');
        $router->get('modules/{moduleName}', 'AsteriskModulesController@show');
        $router->delete('modules/{moduleName}', 'AsteriskModulesController@destroy');
        $router->post('modules/{moduleName}', 'AsteriskModulesController@create');
        $router->put('modules/{moduleName}', 'AsteriskModulesController@putModule'); //put seems to be the same as post (can't put if already exists, kind of pointless)

        //Logging:
        $router->get('logging', 'AsteriskLoggingController@listLogChannels');
        $router->post('logging/{logChannelName}', 'AsteriskLoggingController@addLog');
        $router->delete('logging/{logChannelName}', 'AsteriskLoggingController@deleteLog');
        $router->put('logging/{logChannelName}/rotate', 'AsteriskLoggingController@rotateLog');

        //Variables:
        $router->get('variable/{variableName}', 'AsteriskVariableController@getGlobalVar');
        $router->post('variable', 'AsteriskVariableController@setGlobalVar');

        $router->group([
            'prefix' => 'config'
        ], function($router) {
            //Dynamic Configuration:
            $router->get('dynamic/{configClass}/{objectType}/{id}', 'AsteriskConfigDynamicController@getObject');
            $router->put('dynamic/{configClass}/{objectType}/{id}', 'AsteriskConfigDynamicController@putModule');
            $router->delete('dynamic/{configClass}/{objectType}/{id}', 'AsteriskConfigDynamicController@deleteObject');
        });

    });

    //Channels:
    $router->get('channels', 'ChannelsController@list');
    $router->get('channels/{channel}', 'ChannelsController@get');
    $router->get('channels/{channel}/variable/{variable}', 'ChannelsController@getChannelVar');
    $router->get('channels/{channel}/rtpstatistics', 'ChannelsController@rtpstatistics');
    $router->post('channels', 'ChannelsController@originate');
    $router->post('channels/externalMedia', 'ChannelsController@externalMedia');
    $router->post('channels/create', 'ChannelsController@create');
    $router->post('channels/{channel}', 'ChannelsController@originateWithId');
    $router->post('channels/{channel}/continue', 'ChannelsController@continueInDialplan');
    $router->post('channels/{channel}/move', 'ChannelsController@move');
    $router->post('channels/{channel}/redirect', 'ChannelsController@redirect');
    $router->post('channels/{channel}/answer', 'ChannelsController@answer');
    $router->post('channels/{channel}/ring', 'ChannelsController@ring');
    $router->post('channels/{channel}/dtmf', 'ChannelsController@sendDTMF');
    $router->post('channels/{channel}/mute', 'ChannelsController@mute');
    $router->post('channels/{channel}/hold', 'ChannelsController@hold');
    $router->post('channels/{channel}/moh', 'ChannelsController@startMoh');
    $router->post('channels/{channel}/silence', 'ChannelsController@startSilence');
    $router->post('channels/{channel}/play', 'ChannelsController@play');
    $router->post('channels/{channel}/play/{playbackId}', 'ChannelsController@playWithId');
    $router->post('channels/{channel}/record', 'ChannelsController@record');
    $router->post('channels/{channel}/variable', 'ChannelsController@setChannelVar');
    $router->post('channels/{channel}/snoop', 'ChannelsController@snoopChannel');
    $router->post('channels/{channel}/snoop/{snoopId}', 'ChannelsController@snoopChannelWithId');
    $router->post('channels/{channel}/dial', 'ChannelsController@dial');
    $router->delete('channels/{channel}/ring', 'ChannelsController@ringStop');
    $router->delete('channels/{channel}', 'ChannelsController@hangup');
    $router->delete('channels/{channel}/mute', 'ChannelsController@unmute');
    $router->delete('channels/{channel}/hold', 'ChannelsController@unhold');
    $router->delete('channels/{channel}/moh', 'ChannelsController@stopMoh');
    $router->delete('channels/{channel}/silence', 'ChannelsController@stopSilence');

    //Endpoints:
    $router->get('endpoints', 'EndpointsController@list');
    $router->get('endpoints/{tech}', 'EndpointsController@listByTech');
    $router->get('endpoints/{tech}/{resource}', 'EndpointsController@get');
    $router->put('endpoints/sendMessage', 'EndpointsController@sendMessage');
    $router->put('endpoints/{tech}/{resource}/sendMessage', 'EndpointsController@sendMessageToEndpoint');

    //Bridges:
    $router->get('bridges', 'BridgesController@list');
    $router->get('bridges/{bridgeId}', 'BridgesController@get');
    $router->delete('bridges/{bridgeId}', 'BridgesController@destroy');
    $router->post('bridges', 'BridgesController@create');
    $router->post('bridges/{bridgeId}', 'BridgesController@createWithId');
    $router->post('bridges/{bridgeId}/addChannel', 'BridgesController@addChannel');
    $router->post('bridges/{bridgeId}/removeChannel', 'BridgesController@removeChannel');
    $router->post('bridges/{bridgeId}/videoSource/{channelId}', 'BridgesController@setVideoSource');
    $router->delete('bridges/{bridgeId}/videoSource', 'BridgesController@clearVideoSource');
    $router->post('bridges/{bridgeId}/moh', 'BridgesController@startMoh');
    $router->delete('bridges/{bridgeId}/moh', 'BridgesController@stopMoh');
    $router->post('bridges/{bridgeId}/play', 'BridgesController@play');
    $router->post('bridges/{bridgeId}/play/{playbackId}', 'BridgesController@playWithId');
    $router->post('bridges/{bridgeId}/record', 'BridgesController@record');

    //Applications:
    $router->get('applications', 'ApplicationsController@list');
    $router->get('applications/{applicationName}', 'ApplicationsController@get');
    $router->post('applications/{applicationName}/subscription', 'ApplicationsController@subscribe');
    $router->put('applications/{applicationName}/eventFilter', 'ApplicationsController@filter');
    $router->delete('applications/{applicationName}/subscription', 'ApplicationsController@unsubscribe');

    //DeviceStates:
    $router->get('deviceStates', 'DeviceStatesController@list');
    $router->get('deviceStates/{deviceName}', 'DeviceStatesController@get');
    $router->put('deviceStates/{deviceName}', 'DeviceStatesController@update');
    $router->delete('deviceStates/{deviceName}', 'DeviceStatesController@delete');

    //Events:
    $router->get('events', 'EventsController@eventWebsocket');
    $router->post('events/user/{eventName}', 'EventsController@userEvent');

    //Mailboxes:
    $router->get('mailboxes', 'MailboxesController@list');
    $router->get('mailboxes/{mailboxName}', 'MailboxesController@get');
    $router->put('mailboxes/{mailboxName}', 'MailboxesController@update');
    $router->delete('mailboxes/{mailboxName}', 'MailboxesController@delete');

    //Playbacks:
    $router->get('playbacks/{playbackId}', 'PlaybacksController@get');
    $router->delete('playbacks/{playbackId}', 'PlaybacksController@stop');
    $router->post('playbacks/{playbackId}/control', 'PlaybacksController@control');

});
